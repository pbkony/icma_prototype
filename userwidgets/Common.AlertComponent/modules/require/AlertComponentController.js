define(function() {

	return {
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setBindings();
      },
      setBindings:function(){
      },
      dismissAlert:function(){
        this.view.setVisibility(false);
        this.view.flexAlertContainer.top="0%";
      },
      showGreenAlert:function(message){
        this.view.setVisibility(true);
        this.view.flexAlertContainer.skin="sknGreenAlert";
        this.view.imgLeft.src="check_white.png";
        this.view.lblMessage.text=message;
        this.goUpAnimation();
      },
      showRedAlert:function(message){
        this.view.setVisibility(true);
        this.view.flexAlertContainer.skin="sknRedAlert";
        this.view.imgLeft.src="exit_white.png";
        this.view.lblMessage.text=message;
        this.goUpAnimation();
      },
      goUpAnimation:function(){
       var animDefinition = {
          100: {
            top:"-100%"
          }
        };
      	var animDef = kony.ui.createAnimation(animDefinition);
        var config = {
            "duration": 0.5,
            "iterationCount": 1,
            "delay": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS
        };
        var self =this;
      	this.view.flexAlertContainer.animate(animDef, config, {animationEnd:self.dismissAlert});
      }	
	};
});