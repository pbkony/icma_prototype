define(function() {
 	var _onChange=()=>{};
	return {
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setBindings();
        this.changeCalendarText();
//         this.view.postShow=this.onPostShow.bind(this);
      },
      onPostShow:function(){
        
      },
      setBindings:function(){
        this.view.calendarTransparent.onSelection=this.changeCalendarText.bind(this);
      },
      changeCalendarText:function(){
        var calendarComponents=this.view.calendarTransparent.dateComponents;
        var formattedDate=calendarComponents[0]+"-"+calendarComponents[1]+"-"+calendarComponents[2];
        this.view.lblSelectedOption.text=formattedDate;
        _onChange(formattedDate);
      },
      onChange:function(callback){
      	_onChange=callback;
      }
      
      
	};
});