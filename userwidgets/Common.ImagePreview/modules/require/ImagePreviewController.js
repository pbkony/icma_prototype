define(function() {

	return {
      constructor: function(baseConfig, layoutConfig, pspConfig) {
      },
      setBase64:function(rawBytes){
        kony.application.showLoadingScreen("sknPopUpBackground", 
                                           "loading Image..", 
                                           constants.LOADING_SCREEN_POSITION_FULL_SCREEN,
                                           true,
                                           false, {});
       	var imgBase64=kony.convertToBase64(rawBytes);
        this.view.imgAttachmentPreview.base64=imgBase64;
        kony.application.dismissLoadingScreen();
      },
      //Logic for getters/setters of custom properties
      initGettersSetters: function() {

      }
	};
});