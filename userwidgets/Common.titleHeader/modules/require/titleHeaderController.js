define(function() {

	return {
      _prevForm:null,
      constructor: function(baseConfig, layoutConfig, pspConfig) {
      	this.view.flxBack.onClick=this.navigateToLast.bind(this);
        this.view.postShow=this.onPostShow.bind(this);
      },
      resetPrev:function(){
        this._prevForm=null;
      },
      onPostShow:function(){
        kony.application.getCurrentForm().onDeviceBack=this.navigateToLast.bind(this);
        this._prevForm=this._prevForm?this._prevForm:kony.application.getPreviousForm().id;
        if(this.customNavOnBack){
          this.view.flxBack.onClick=this.backNavCustom.bind(this);
        }
      },
      backNavCustom:function(){
        const nav =new kony.mvc.Navigation(this.customNavOnBack);
        nav.navigate();
      },
      navigateToLast:function(){
        var nav= new kony.mvc.Navigation(this._prevForm);
        this.resetPrev();
        nav.navigate();
      }
      
	};
});