define(function() {
	return {
      _status:false,
      _onChange:()=>{},
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setBindings();
      },
      setBindings:function(){
        this.view.btnCheckbox.onClick=this.toggleCheckbox.bind(this);
        this.view.flxCheckbox.onClick=this.toggleCheckbox.bind(this);
      },
      toggleCheckbox:function(){
        if(this._status){
          this.setUnchecked();
        }else{
          this.setChecked();
        }
        this._onChange(this._status);
      },
      getStatus:function(){
        return this._status;
      },
      setChecked:function(){
        this.view.btnCheckbox.skin="sknCheckboxChecked";
        this._status=true;
      },
      setUnchecked:function(){
        this.view.btnCheckbox.skin="sknCheckbox";
        this._status=false;
      },
      onChange:function(callback){
        this._onChange=callback;
      }
	};
});