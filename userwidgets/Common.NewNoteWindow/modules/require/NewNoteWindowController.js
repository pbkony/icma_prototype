define(function() {
	return {
      saveCallback:()=>{},
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setBindings();
      },
      closeWindow:function(){
        this.view.setVisibility(false);
      },
      onSave:function(){
        var currentForm = kony.application.getCurrentForm();
        this.saveCallback();
        this.closeWindow();
        currentForm.NewNote.flxNoteFilled.setVisibility(true);
      },
      setBindings:function(){
        this.view.btnSave.onClick=this.onSave.bind(this);
        this.view.flxExit.onClick=this.closeWindow.bind(this);
        this.view.flxBackground.onClick=()=>{};
      },
      setCustomCloseCallback:function(callback){
      	this.closeWindow=callback;
      },
      setCustomSaveCallback:function(callback){
      	this.saveCallback=callback;
      },
	};
});