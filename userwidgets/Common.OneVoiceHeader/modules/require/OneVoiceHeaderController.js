define(function() {

	return {
		constructor: function(baseConfig, layoutConfig, pspConfig) {
			this.view.flxLogoutIcon.onClick=this.navigateToLogin.bind(this);
		},
      	navigateToLogin: function(){
          var nav = new kony.mvc.Navigation("frmLogin");
          nav.navigate();
        },
		//Logic for getters/setters of custom properties
		initGettersSetters: function() {

		}
	};
});