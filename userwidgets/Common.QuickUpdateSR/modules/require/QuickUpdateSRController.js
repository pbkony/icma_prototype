define(function() {

	return {
      saveCallback:()=>{},
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setInitialConfig();
        this.setBindings();
      },
      setInitialConfig:function(){
        this.view.dropDownResolution.setData(0,SR_RESOLUTION_LIST);
        this.view.dropDownStatus.setData(0,SR_STATUS_LIST);
      },
      setBindings:function(){
        this.view.btnSave.onClick=this.onSave.bind(this);
        this.view.btnCancel.onClick=this.onCancel.bind(this);
        this.view.flxBackground.onClick=()=>{};
      },
      closeWindow:function(){
        this.view.flxWindow.setVisibility(false);
        this.view.flxBackground.setVisibility(false);
      },
      onCancel:function(){
        this.closeWindow();
      },
      onSave:function(){
        this.saveCallback();
        this.closeWindow();
      },
      setCustomCloseCallback:function(callback){
      	this.closeWindow=callback;
      },
      setCustomSaveCallback:function(callback){
      	this.saveCallback=callback;
      },
	};
}); 