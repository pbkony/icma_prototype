define(function() {
	return {
      _opened:false,
      _selectedIndex:0,
      _sgmOptionsData:[],
      _availableOptions:[],
      _onChange:()=>{},
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setBindings();          
       	this.setInitialConfig();
      },
      setBindings:function(){
        this.view.flxSelectedOption.onClick=this.toggleDropdown.bind(this);
        this.view.sgmOptions.onRowClick=this.selectOption.bind(this);
      },
      setInitialConfig:function(){
       	this.view.sgmOptions.widgetDataMap={
          "lblOption":"option"
        };
      },
      setData:function(selectedIndex,optionsArray){
        this._sgmOptionsData=this.convertArrayToSegmentData(optionsArray);
        if(selectedIndex>=0&&selectedIndex<optionsArray.length){
           this._selectedIndex=selectedIndex;
        }
        this.updateDropdown();
      },
      convertArrayToSegmentData:function(optionsArray){
        const sgmData=optionsArray.map((option,index)=>{
          return {option:option.toUpperCase(),index};
        });
        return sgmData;
      },
      updateDropdown:function(){
        var sgmOptionDataCpy=this._sgmOptionsData.slice();
        var selectedOption=sgmOptionDataCpy.splice(this._selectedIndex,1);
        this._availableOptions=sgmOptionDataCpy;
        this.view.lblSelectedOption.text=selectedOption[0]&&selectedOption[0].option;
        this.view.sgmOptions.setData(sgmOptionDataCpy);
      },
      
      selectOption:function(){
        const selectedRow= this.view.sgmOptions.selectedRowItems[0];
        this._selectedIndex= selectedRow.index;
        this.updateDropdown();
        this.closeDropdown();
        this._onChange(selectedRow);
      },
      onChange:function(callback){
        this._onChange=callback;
      },
      getActualOption:function(){
        return this._selectedIndex;
      },
      closeDropdown:function(){
        var self=this;
        this._opened=false;
        this.view.sgmOptions.setVisibility(false);
      },
      openDropdown:function(){
        var self=this;
        this._opened=true;
        this.view.sgmOptions.setVisibility(true);
        
      },
      toggleDropdown:function(){
        if(this._opened){
            this.closeDropdown();
        }
        else{
            this.openDropdown();
        }
      },
	};
});