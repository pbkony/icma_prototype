define(function() {
	return {
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setBindings();
      },
      onPostShow:function(){
        
      },
      setBindings:function(){
		this.view.flxNewNote.onClick=this.openNewNoteWindow.bind(this);
      },
      openNewNoteWindow:function(){
        var NNWindow=new Common.NewNoteWindow({
          "clipBounds": true,
          "isMaster": true,
          "height": "100%",
          "id": "networkError",
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "slFbox",
          "top": "0dp",
          "width": "100%",
          "zIndex": 12
        }, {}, {});
        var currentForm=kony.application.getCurrentForm();
        currentForm.add(NNWindow);
      }
	};
});