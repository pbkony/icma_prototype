define(function() {

	return {
      _index:0,
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setBindings();
      },
      setBindings:function(){
        this.view.flxAddAttachment.onClick=this.addAttachment.bind(this);
      },
      addAttachment:function(){
		var querycontext = {mimeType:"image/*"};
        var self=this;
        try{
          kony.phone.openMediaGallery(self.onselectioncallback.bind(self), querycontext);
		}catch(e){
          alert(e);
        }
      },
      onselectioncallback:function(rawbytes){
        if (rawbytes === null){
          return;
        }
        if(this._index>=3){
          this.view.flxViewMore.setVisibility(true);
          return;
        }
        var self=this;
        var ImagePreview=new Common.ImagePreview({
          "clipBounds": true,
          "isMaster": true,
          "height": "66dp",
          "id": "ImagePreview"+self._index,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "flxRounded",
          "top": "0dp",
          "width": "59dp",
          "zIndex": 1
        }, {}, {});
        if(self._index){
          this.view["ImagePreview"+(self._index-1)].left="15dp";
        }else{
          this.view.flxAddAttachment.left="15dp";
        }
        this.view.flxAttachments.addAt(ImagePreview,0);
        ImagePreview.setBase64(rawbytes);
        this._index++;     
      }
    };
});