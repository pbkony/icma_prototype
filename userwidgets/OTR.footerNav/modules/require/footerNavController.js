define(function() {

	return {
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.view.postShow=this.onPostShow.bind(this);
        this.view.flxAccounts.onClick=this.goToMeetings.bind(this);
        this.view.flxActivities.onClick=this.goToActivities.bind(this);
      },
      onPostShow: function(){
        if(this.actualScreen=="meetings"){
          this.view.lblAccounts.skin="lblActive";
          this.view.imgAccounts.src="home_active.png";
          this.view.lblActivities.skin="lblInactive";
          this.view.imgActivities.src="activities_inactive.png";
        }else{
          this.view.lblAccounts.skin="lblInactive";
          this.view.imgAccounts.src="home_inactive.png";
          this.view.lblActivities.skin="lblActive";
          this.view.imgActivities.src="activities_active.png";
        }
      },
      goToMeetings:function(){
        var nav = new kony.mvc.Navigation("frmOTRMeetings");
        nav.navigate();
      },
      goToActivities:function(){
        var nav = new kony.mvc.Navigation("frmOTRActivities");
        nav.navigate();
      },

      //Logic for getters/setters of custom properties
      initGettersSetters: function() {

      }
	};
});