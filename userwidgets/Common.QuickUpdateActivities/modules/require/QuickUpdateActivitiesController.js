define(function() {

	return {
      saveCallback:()=>{},
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.setInitialConfig();
        this.setBindings();
      },
      setInitialConfig:function(){
        this.view.Dropdown.setData(0,ACTIVITIES_STATUS_LIST);
      },
      setBindings:function(){
        this.view.btnSave.onClick=this.onSave.bind(this);
        this.view.btnCancel.onClick=this.onCancel.bind(this);
        this.view.flxBackground.onClick=()=>{};
      },
      onCancel:function(){
      	this.closeWindow();
      },
      closeWindow:function(){
        this.view.flxWindow.setVisibility(false);
        this.view.flxBackground.setVisibility(false);
      },
      onSave:function(){
        this.saveCallback();
        this.closeWindow();
      },
      setCustomCloseCallback:function(callback){
      	this.closeWindow=callback;
      },
      setCustomSaveCallback:function(callback){
      	this.saveCallback=callback;
      },
	};
}); 