define(function() {

	return {
      _tab1Callback:()=>{},
      _tab2Callback:()=>{},
      constructor: function(baseConfig, layoutConfig, pspConfig) {
        this.view.postShow=this.onPostShow.bind(this);
      },
      onPostShow:function(){
        if(this.activeIndex==1){
          this.view.richTxtTab1.skin="sknRichTxtTitleActive";
          this.view.richTxtTab2.skin="sknRichTxtTitle";
          this.view.flxActiveTab1.setVisibility(true);
          this.view.flxActiveTab2.setVisibility(false);
        }else{
          this.view.richTxtTab2.skin="sknRichTxtTitleActive";
          this.view.richTxtTab1.skin="sknRichTxtTitle";
          this.view.flxActiveTab2.setVisibility(true);
          this.view.flxActiveTab1.setVisibility(false);
        }
        this.setBindings();
      },
      setBindings:function(){
        if(this.type=="one form"){
            this.view.flxTab1.onClick=this.focusTab1.bind(this);
        	this.view.flxTab2.onClick=this.focusTab2.bind(this);
        }else{
          this.view.flxTab1.onClick=this.navToTab1.bind(this);
          this.view.flxTab2.onClick=this.navToTab2.bind(this);
        }
      },
      focusTab1:function(){
        this.view.richTxtTab1.skin="sknRichTxtTitleActive";
        this.view.richTxtTab2.skin="sknRichTxtTitle";
        this.view.flxActiveTab1.setVisibility(true);
        this.view.flxActiveTab2.setVisibility(false);
        this._tab1Callback();
      },
      setCallbackTab1(callback){
        this._tab1Callback=callback;
      },
      focusTab2:function(){
        this.view.richTxtTab2.skin="sknRichTxtTitleActive";
        this.view.richTxtTab1.skin="sknRichTxtTitle";
        this.view.flxActiveTab2.setVisibility(true);
        this.view.flxActiveTab1.setVisibility(false);
        this._tab2Callback();
      },
      setCallbackTab2(callback){
        this._tab2Callback=callback;
      },
      navToTab1:function(){
        const nav=new kony.mvc.Navigation(this.tabOneNav);
        nav.navigate();
      },
      navToTab2:function(){
        const nav=new kony.mvc.Navigation(this.tabTwoNav);
        nav.navigate();
      },
      
      //Logic for getters/setters of custom properties
      initGettersSetters: function() {

    }
	};
});