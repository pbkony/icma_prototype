define({ 
  onNavigate:function(){
    this.setBindings();
    this.view.preShow=this.onPreShow.bind(this);
  },
  onPreShow:function(){
    this.setAddress("Mayfield, KY, 42066");
    this.setSgmWSSEsData();
    this.setSgmOSSsData();
  },
  setBindings:function(){
    this.view.TabNav.setCallbackTab1(this.showOSSInformation.bind(this));
    this.view.TabNav.setCallbackTab2(this.showWSEE.bind(this));
  },
  showWSEE:function(){
	this.view.flxWSEE.setVisibility(true);
    this.view.flxOOSInformation.setVisibility(false);
  },
  showOSSInformation:function(){
	this.view.flxWSEE.setVisibility(false);
    this.view.flxOOSInformation.setVisibility(true);
  },
  setSgmOSSsData:function(){
    this.view.sgmOSSs.widgetDataMap={ 
      "lblSection": "position", 
      "lblContactName": "name", 
      "btnPhone": "phone",
    };
    var data = [
      { name: "Danny Tate", phone: "209-536-2513"},
      { name: "Melvin Richards", phone: "530-838-8684"},
      { name: "Julia Fox", phone: "590-423-9382"},
      { name: "Don Davis", phone: "933-824-8784"},
      { name: "Minnie Thomas", phone: "824-389-5183"},
      { name: "Carrie Wallace", phone: "917-290-2144"}
    ];
      
    this.view.sgmOSSs.setData(data); 
  },
  setSgmWSSEsData:function(){
    this.view.sgmWSEEs.widgetDataMap={ 
      "lblName": "name", 
      "lblStatus": "status", 
      "lblResidence": "residence",
      "lblStatusDate":"statusDate",
      "lblClassification":"classification",
      "lblID":"id"
    };
    var data = [
      { 
        name: "Alex Sion",
        status:"Hired",
        residence:"TX",
        id:"284430022",
        statusDate:"01/05/2017",
        classification:"Seasonal"
      },
      { 
        name: "Ben Cortez",
        status:"Rehired",
        residence:"GA",
        id:"284430023",
        statusDate:"01/05/2017",
        classification:"Seasonal"
      },
      { 
        name: "Dan Marous",
        status:"LOA",
        residence:"CA",
        id:"284430024",
        statusDate:"01/05/2017",
        classification:"Part time"
      },
      { 
        name: "Dawn Edwards",
        status:"Hired",
        residence:"GA",
        id:"284430023",
        statusDate:"01/05/2017",
        classification:"Part Time"
      },
      { 
        name: "Elizabeth Williams",
        status:"LOA",
        residence:"GA",
        id:"284430024",
        statusDate:"01/05/2017",
        classification:"Full Time"
      }
    ];
      
    this.view.sgmWSEEs.setData(data); 
  },
  setAddress:function(address){
    this.view.richTxtAddress.text="<a href=\"https://www.google.com/maps/search/?api=1&query="+kony.net.urlEncode(address)+"\"><u>"+address+"</u></a>";
  	this.view.richTxtAddress.skin="sknAccountDetailsData";
  }
 });