define({ 
  _submit:false,
  onNavigate: function(params){
    this.setSegmentData();
    this.setBindings();
    this._submit=params&&params.submitSuccess;
    this.view.postShow=this.onPostShow.bind(this);
  },
  onPostShow:function(){
    if(this._submit){
      this.view.AlertComponent.showGreenAlert("Your activity has been successfuly submitted!");
	}
  },
  setBindings:function(){
    this.view.sgmActivities.onRowClick=this.navigateToActivityDetail.bind(this);
  },
  navigateToActivityDetail:function(){
    var nav= new kony.mvc.Navigation("frmOTRActivityDetail");
    nav.navigate();
  },
  setSegmentData: function() {
    this.view.sgmActivities.widgetDataMap={
      "lblTitle": "title",
      "lblArea": "area",
      "lblSubArea": "subarea",
      "lblStart": "start", 
      "lblDueDate":"duedate",
      "lblStatus":"status",
    };
    var data = [
      {
        title: "ABC Widgets - 3001200",
        area: "Client Management", 
        subarea:"Quality Proactive Touch Point",
        start: "09-10-2017", 
        duedate:"11-15-2017", 
        status:"In progress",
      }, {
        title: "Access Speciality Animal Hospital - 200384",
        area: "Client Management", 
        subarea:"Quality Proactive Touch Point",
        start: "09-16-2017", 
        duedate:"11-15-2017", 
        status:"In progress",
      }, {
        title: "Complete Pharmacy Care - 293721",
        area: "Client Management", 
        subarea:"Quality Proactive Touch Point",
        start: "09-29-2017", 
        duedate:"11-12-20177", 
        status:"In progress",
      }, 
    ]; 
    this.view.sgmActivities.setData(data); 
  },


});