define({ 
  onNavigate:function(){
    this.setSegmentData();
  },

  setSegmentData: function() {
    this.view.sgmContacts.widgetDataMap={ 
      "lblSection": "position", 
      "lblContactName": "name", 
      "btnPhone": "phone",
      "btnEmail":"email"
    };
    var self=this;
    var data = [
      [{ position: "MHRS(Manager, HR services)" }, 
       [{ name: "Kathryn Santiago", phone: "412-473-0689", email:"kathrynsantiagon@gmail.com" }]
      ], 
      [{ position: "Client Liaison" }, 
       [{ name: "Tillie Graham", phone: "034-506-6864", email:"tilliegraham@gmail.com" }]
      ],
      [{ position: "HR Specialist" }, 
       [{ name: "Carolyn Flowers", phone: "011-263-9624", email:"carolynflowers@gmail.com" }]
      ],
      [{ position: "Payroll Specialist" }, 
       [{ name: "Herbert Collins", phone: "467-593-8062", email:"herbertcollins@gmail.com" }]
      ],
      [{ position: "401 (K) Plan Consultant" }, 
       [{ name: "Aiden Brown", phone: "587-387-3148", email:"aidenbrown@gmail.com" }]
      ]
    ]; 
    this.view.sgmContacts.setData(data); 
  },
//   getEmailLink:function(email){
//     return "<a href=\"mailto:"+email+"\">O</a>";
//   }

 });