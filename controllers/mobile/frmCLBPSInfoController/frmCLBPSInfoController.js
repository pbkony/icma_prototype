define({ 
  onNavigate:function(){
    this.setSegmentData();
  },

  setSegmentData: function() {
    this.view.sgmBPS.widgetDataMap={ 
      "lblName": "name", 
      "lblFirstInvoice": "firstInvoice",
      "lblRecentInvoice": "recentInvoice",
      "lblStatus":"status"
    };
    var self=this;
    var data = [
      { name: "Perform Smart", firstInvoice: "11/05/2017", recentInvoice:"14/05/2018",status:"Active" },
      { name: "Expensable Corporate", firstInvoice: "26/05/2017", recentInvoice:"03/06/2018",status:"Terminated" },
      { name: "Growth Force", firstInvoice: "05/08/2017", recentInvoice:"18/09/2018",status:"Open" },
      { name: "Time Star", firstInvoice: "28/10/2017", recentInvoice:"02/11/2018",status:"Open" }
    ]; 
    this.view.sgmBPS.setData(data); 
  },
});
  