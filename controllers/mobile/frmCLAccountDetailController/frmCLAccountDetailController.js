define({ 
  onNavigate:function(){
	this.setBindings();
    this.view.preShow=this.onPostShow.bind(this);
  },
  onPostShow:function(){
    this.setPhone("541 754 3010");
    this.setAddress("Mayfield, KY, 42066");
  },
  setBindings:function(){
    this.view.flxInformation.onClick=this.navToAccountInfo.bind(this);
    this.view.flxContacts.onClick=this.navToContacts.bind(this);
    this.view.flxTeam.onClick=this.navToTeam.bind(this);
    this.view.flxWSEEs.onClick=this.navToWSEEs.bind(this);
    this.view.flxBPSInfo.onClick=this.navToBPSInfo.bind(this);
    this.view.flxRelatedAccounts.onClick=this.navToRelatedAccounts.bind(this);
    this.view.flxOpenSRs.onClick=this.navToOpenActivities.bind(this);
    this.view.flxClosedSRs.onClick=this.navToClosedActivities.bind(this);
  },
  navToOpenActivities:function(){
    const nav= new kony.mvc.Navigation("frmCLOpenActivities");
    nav.navigate();
  },
  navToClosedActivities:function(){
    const nav= new kony.mvc.Navigation("frmCLClosedActivities");
    nav.navigate();
  },
  navToRelatedAccounts:function(){
    var nav = new kony.mvc.Navigation("frmCLRelatedAccounts");
    nav.navigate();
  },
  navToBPSInfo:function(){
    var nav = new kony.mvc.Navigation("frmCLBPSInfo");
    nav.navigate();
  },
  navToWSEEs:function(){
    var nav = new kony.mvc.Navigation("frmCLWSEEs");
    nav.navigate();
  },
  navToAccountInfo:function(){
    var nav = new kony.mvc.Navigation("frmCLAccountInfo");
    nav.navigate();
  },
   navToContacts:function(){
    var nav = new kony.mvc.Navigation("frmCLAccountContacts");
    nav.navigate();
  },
  navToTeam:function(){
    var nav = new kony.mvc.Navigation("frmCLAccountTeam");
    nav.navigate();
  },
  setPhone:function(phone){
    this.view.richTxtPhone.text="<a href=\"tel\:+"+kony.net.urlEncode(phone)+"\"><u>"+phone+"</u></a>";
    this.view.richTxtPhone.onClick=this.getPhoneFunction(phone);
    this.view.richTxtPhone.skin="sknAccountDetailsData";
  },
  getPhoneFunction:function(phone){
    return (function(){
      kony.phone.dial(phone);
    });
  },
  setAddress:function(address){
    this.view.richTxtAddress.text="<a href=\"https://www.google.com/maps/search/?api=1&query="+kony.net.urlEncode(address)+"\"><u>"+address+"</u></a>";
  	this.view.richTxtAddress.skin="sknAccountDetailsData";
  }
  

 });