define({
  
  onNavigate: function(){
    this.setInfoSegmentData();
    this.setDropdownData();
    this.setBindings();
  },
  setBindings:function(){
    this.view.btnSubmit.onClick=this.submitActivity.bind(this);
    this.view.btnSave.onClick=this.saveActivity.bind(this);
  },
  saveActivity:function(){
    var nav= new kony.mvc.Navigation("frmCLActivities");
    nav.navigate();
  },
  submitActivity:function(){
    var nav= new kony.mvc.Navigation("frmCLActivities");
    nav.navigate({submitSuccess:true});
  },
  setInfoSegmentData: function() {
    this.view.sgmInfo.widgetDataMap={
      "lblTitle": "title",
      "lblValue": "value",
    };
    var data = [
      {title: "Area", value: "Client Management"},
      {title: "Sub area", value: "Quality Proactive Touch Point"},
      {title: "Start date", value: "09-10-2017"},
      {title: "Contact", value: "Elnora Armstrong"},
    ]; 
    this.view.sgmInfo.setData(data); 
  },
  
  setDropdownData:function() {
    this.view.Dropdown.setData(0,[
      "DONE",
      "UNSCHEDULED",
      "SCHEDULED",
      "IN PROGRESS",
      "ASSIGNED"
    ]);
  }
  
});