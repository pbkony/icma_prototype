define({ 
  onNavigate:function(){
    this.setSegmentData();
  },
  setSegmentData: function() {
    this.view.sgmContacts.widgetDataMap={ 
      "lblSection": "position", 
      "lblContactName": "name", 
      "btnPhone": "phone",
      "btnEmail":"email"
    };
    var data = [
      [{ position: "Owner" }, 
       [{ name: "Hunter Newton", phone: "372-943-7453", email:"hunternewton@gmail.com" }]
      ], 
      [{ position: "OSS (On-Site Supervisor)" }, 
       [{ name: "Keith Ballard", phone: "705-087-8096", email:"keithballard@gmail.com" },
       { name: "Randy Allen", phone: "781-562-8583", email:"randyallen@gmail.com" },
       { name: "Essie Beck", phone: "816-246-4247", email:"essiebeck@gmail.com" }]
      ],
      [{ position: "Payroll Contact" }, 
       [{ name: "William Horton", phone: "225-623-6967", email:"williamhorton@gmail.com" },
       { name: "theodore Hamptom", phone: "885-759-6454", email:"theodorehamptom@gmail.com" }]
      ],
      [{ position: "KDM (Key Decision Maker)" }, 
       [{ name: "Robert Logan", phone: "021-991-3149", email:"robertlogan@gmail.com" },
       { name: "Lenora Gross", phone: "488-516-3151", email:"lenoragross@gmail.com" },
       { name: "Bess Chavez", phone: "527-723-9782", email:"besschavez@gmail.com" },
       { name: "Iva Ball", phone: "435-264-3775", email:"ivaball@gmail.com" },
       { name: "Vincent Thornton", phone: "010-581-1368", email:"vincentthorton@gmail.com" }]
      ]
    ]; 
    this.view.sgmContacts.setData(data); 
  },

 });