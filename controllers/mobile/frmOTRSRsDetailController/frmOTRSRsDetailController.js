define({
  
  onNavigate: function(){
    this.setInfoSegmentData();
    this.onPreShow();
    this.setBindings();
  },
  setBindings:function(){
    this.view.btnSubmit.onClick=this.submitSR.bind(this);
    this.view.btnSave.onClick=this.saveSR.bind(this);
    this.view.flxInfo.onClick=this.navigateToAccountDetail.bind(this);
  },
  navigateToAccountDetail:function(){
    var nav=new kony.mvc.Navigation("frmOTRAccountDetail");
    nav.navigate();
  },
  saveSR:function(){
    var nav= new kony.mvc.Navigation("frmOTRSRs");
    nav.navigate();
  },
  submitSR:function(){
    var nav= new kony.mvc.Navigation("frmOTRSRs");
    nav.navigate();
  },
  setInfoSegmentData: function() {
    this.view.sgmInfo.widgetDataMap={
      "lblTitle": "title",
      "lblValue": "value",
    };
    var data = [
      {title: "Area", value: "COBRA"},
      {title: "Sub area", value: "Takeover Verify & Submission"},
      {title: "Group", value: "Name of the group"},
      {title: "Open date", value: "09-10-2017"},
      {title: "Summary", value: "Here goes a little description, here goes a little description."},
    ]; 
    this.view.sgmInfo.setData(data); 
  },
  
  onPreShow:function() {
    this.setDataDropdownStatus();
    this.setDataDropdownSubStatus();
    this.setDataDropdownResolution();
  },
  
  setDataDropdownStatus: function(){
    this.view.DropdownStatus.setData(0,SR_STATUS_LIST);
  },
  
  setDataDropdownSubStatus: function(){
    this.view.DropdownSubStatus.setData(0,SR_SUBSTATUS_LIST);
  },
  
  setDataDropdownResolution: function(){
    this.view.DropdownResolution.setData(0,SR_RESOLUTION_LIST);
  }
  
});