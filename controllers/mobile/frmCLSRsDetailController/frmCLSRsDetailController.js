define({
  
  onNavigate: function(){
    this.setInfoSegmentData();
    this.onPreShow();
    this.setBindings();
  },
  setBindings:function(){
    this.view.btnSubmit.onClick=this.submitSR.bind(this);
    this.view.btnSave.onClick=this.saveSR.bind(this);
  },
  saveSR:function(){
    var nav= new kony.mvc.Navigation("frmCLSRs");
    nav.navigate();
  },
  submitSR:function(){
    this.view.AlertComponent.showRedAlert("You can't submit an SR under your current criteria");
  },
  setInfoSegmentData: function() {
    this.view.sgmInfo.widgetDataMap={
      "lblTitle": "title",
      "lblValue": "value",
    };
    var data = [
      {title: "Area", value: "Service Operations"},
      {title: "Sub area", value: "Request Customized Report"},
      {title: "Detail", value: "Deatil content"},
      {title: "Group", value: "Group content"},
      {title: "Open date", value: "09-10-2017"},
      {title: "Summary", value: "Here goes a little description, here goes a little description."},
    ]; 
    this.view.sgmInfo.setData(data); 
  },
  
  onPreShow:function() {
    this.setDataDropdownStatus();
    this.setDataDropdownSubStatus();
    this.setDataDropdownResolution();
  },
  
  setDataDropdownStatus: function(){
    this.view.DropdownStatus.setData(0,SR_STATUS_LIST);
  },
  
  setDataDropdownSubStatus: function(){
    this.view.DropdownSubStatus.setData(0,SR_SUBSTATUS_LIST);
  },
  
  setDataDropdownResolution: function(){
    this.view.DropdownResolution.setData(0,SR_RESOLUTION_LIST);
  }
  
});