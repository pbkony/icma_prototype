define({ 

  onNavigate: function(){
    this.setSegmentData();
    this.setBindings();
  },
  setBindings:function(){
    this.view.sgmSRs.onRowClick=this.navigateToSRsDetail.bind(this);
    this.view.flxAdd.onClick=this.navigateToQuickAdd.bind(this);
  },
  navigateToSRsDetail:function(){
    var nav= new kony.mvc.Navigation("frmCLSRsDetail");
    nav.navigate();
  },
  navigateToQuickAdd:function(){
    var nav= new kony.mvc.Navigation("frmQuickAdd");
    nav.navigate("srs");
  },
  setSegmentData: function() {
    this.view.sgmSRs.widgetDataMap={
      "lblTitle": "title",
      "lblArea": "area",
      "lblSubArea": "subarea",
      "lblSubStatus": "substatus", 
      "lblOpenDate":"opendate",
      "lblStatus":"status",
    };
    var self=this;
    var data = [
      {
        title: "ABC Widgets - 3001200",
        area: "COBRA", 
        subarea:"Takeover Verify & Submission",
        substatus: "ABU- Enrolled", 
        opendate:"09-10-2017", 
        status:"Pending",
      }, {
        title: "Access Speciality Animal Hospital - 200384",
        area: "COBRA", 
        subarea:"Takeover Verify & Submission",
        substatus: "Approved", 
        opendate:"9-16-2017", 
        status:{text: "Open", skin: "sknBlueStatus"},
      }, {
        title: "Complete Pharmacy Care - 293721",
        area: "Service Operations", 
        subarea:"Request Customized Report",
        substatus: "Approved", 
        opendate:"9-08-2017", 
        status:{text: "Open", skin: "sknBlueStatus"},
      }, {
        title: "Electro Accoustics & Video - 9900023",
        area: "COBRA", 
        subarea:"Takeover Verify & Submission",
        substatus: "Fulfilled", 
        opendate:"09-29-2017", 
        status:"Pending",
      }, {
        title: "FNB Bank - 2001290",
        area: "Service Operations", 
        subarea:"Request Customized Report",
        substatus: "Payroll VM", 
        opendate:"09-10-2017", 
        status:"Pending",
      }
    ]; 
    this.view.sgmSRs.setData(data); 
  },


});