define({ 
  onViewCreated:function(eObj){
    this.view.addGestureRecognizer(constants.GESTURE_TYPE_SWIPE,{fingers: 1},this.swipeGestureHandler);
  },
  swipeGestureHandler:function(widget,gestureInfo,context){
    var swipeDirection = gestureInfo.swipeDirection;
    if(swipeDirection==1){
      this.showCloseActivity(widget);
    }else if(swipeDirection==2){
      this.hideCloseActivity(widget);
    }
  },
  showCloseActivity:function(widget){
	var animDefinition = {
      100: {
        left:"-30%"
      }
    };
    var animDef = kony.ui.createAnimation(animDefinition);
    var config = {
      "duration": 0.5,
      "iterationCount": 1,
      "delay": 0,
      "fillMode": kony.anim.FILL_MODE_FORWARDS
    };
    var self =this;
    widget.flxContainer.animate(animDef, config, null);
  },
  hideCloseActivity:function(widget){
    var animDefinition = {
      100: {
        left:"0%"
      }
    };
    var animDef = kony.ui.createAnimation(animDefinition);
    var config = {
      "duration": 0.5,
      "iterationCount": 1,
      "delay": 0,
      "fillMode": kony.anim.FILL_MODE_FORWARDS
    };
    var self =this;
    widget.flxContainer.animate(animDef, config, null);
  }
  
 });