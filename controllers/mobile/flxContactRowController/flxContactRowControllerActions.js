define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnPhone **/
    AS_Button_h0e0effb6f3144daba88a1c2af3e4eb4: function AS_Button_h0e0effb6f3144daba88a1c2af3e4eb4(eventobject, context) {
        var self = this;
        return self.onPhoneClick.call(this, eventobject);
    },
    /** onClick defined for btnEmail **/
    AS_Button_df8d2e8a0f4c4d5ca0963f698817b4e8: function AS_Button_df8d2e8a0f4c4d5ca0963f698817b4e8(eventobject, context) {
        var self = this;
        return self.onEmailClick.call(this, eventobject);
    }
});