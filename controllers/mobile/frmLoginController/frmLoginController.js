define({ 
  _rememberMe:true,
  _isRendered:false,
  onNavigate:function(){
	this.view.postShow=this.onPostShow.bind(this);
    this.view.preShow=this.onPreShow.bind(this);
  },
  onPreShow:function(){
   	this.setBindings();
    if(!this._isRendered){
      this.view.flxPeopleOverlay.opacity=0;
    }
  },
  onPostShow:function(){
    if(!this._isRendered){
      this.imgPeopleAnimation();
      this.flxLoginDataAnimation();
      this.flxOneVoiceLogoAnimation();
      this._isRendered=true;
    }
    
  },
  setBindings:function(){
    this.view.btnRememberMe.onClick=this.toggleRememberMe.bind(this);
    this.view.btnLogin.onClick=this.goToNextScreen.bind(this);
  },
  goToNextScreen:function(){
    var nextForm="frmCLAccounts";
    var email=this.view.txtBoxEmail.text;
    if(email==="otr"){
      nextForm="frmOTRMeetings";
    }
    var nav = new kony.mvc.Navigation(nextForm);
	nav.navigate();
  },
  toggleRememberMe(){
    this._rememberMe=!this._rememberMe;
    this.view.btnRememberMe.skin=this._rememberMe?"sknCheckboxChecked":"sknCheckbox";
  },
  imgPeopleAnimation:function(){
    var animationObj=kony.ui.createAnimation({
      0:{
        "opacity":0,
      },
      100:{
        "opacity":1,
      }
    });
    var animationConfig={
      "delay":1.5,
      "duration":1,
      "fillMode":kony.anim.FILL_MODE_FORWARDS
    };
    var self=this;
    this.view.flxPeopleOverlay.animate(animationObj, animationConfig, {
      "animationEnd":self.hidePeople.bind(self)
    });
  },
  hidePeople:function(){
    this.view.flxPeopleImage.setVisibility(false);
  },
  flxLoginDataAnimation:function(){
    var animationObj=kony.ui.createAnimation({
      0:{
        "opacity":1,
      },
      100:{
        "opacity":0,
      }
    });
    var animationConfig={
      "delay":2.5,
      "duration":1,
      "fillMode":kony.anim.FILL_MODE_FORWARDS
    };
    this.view.flxFormOverlay.animate(animationObj, animationConfig, null);
  },
  flxOneVoiceLogoAnimation:function(){
  	var animationObj=kony.ui.createAnimation({
      "100":{
        "top":"73dp",
      }
    });
    var animationConfig={
      "delay":1.5,
      "duration":1,
      "fillMode":kony.anim.FILL_MODE_FORWARDS
    };
    this.view.flxOneVoiceLogo.animate(animationObj, animationConfig, null);
  }
  
 });