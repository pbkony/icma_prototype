define({ 
   onNavigate: function () { 
     this.setSegmentData();
     this.setBindings();
   }, 
   setBindings:function(){
     this.view.SearchBar.txtBoxSearch.onTextChange=this.searchSegments.bind(this);
     this.view.sgmAccounts.onRowClick=this.navigateToAccountDetail.bind(this);
   },
  navigateToAccountDetail:function(sgmAccounts){
    var selectedAccount=sgmAccounts.selectedRowItems[0];
    var nav= new kony.mvc.Navigation("frmCLAccountDetail");
    nav.navigate(selectedAccount);
  },
   setSegmentData: function() {
     this.view.sgmAccounts.widgetDataMap={ 
       "lblFirstLetter": "firstLetter", 
       "lblAccountName": "accountName", 
       "lblAccountCode": "accountCode" };
     var data = [
       [{ firstLetter: "A" }, 
        [{ accountName: "ABCWidgets", accountCode: "3001200" }, 
         { accountName: "Access Speciality Animal Hospital", accountCode: "200384" },
         { accountName: "Airway Management", accountCode: "937730" }]
       ], 
       [{ firstLetter: "C" }, 
        [{ accountName: "Complete Pharmacy Care", accountCode: "293721" }]
       ],
       [{ firstLetter: "E" }, 
        [{ accountName: "Electro Accousstics & Video", accountCode: "9900023" }]
       ],
       [{ firstLetter: "F" }, 
        [{ accountName: "FNB Bank", accountCode: "2001290" }]
       ],
       [{ firstLetter: "N" }, 
        [{ accountName: "Nortech Systems", accountCode: "900029" }]
       ],
       [{ firstLetter: "S" }, 
        [{ accountName: "Sonora Tech", accountCode: "304850" },
        { accountName: "Stratitia", accountCode: "100283" }]
       ],
       [{ firstLetter: "T" }, 
        [{ accountName: "Texas Healthcare Linen", accountCode: "937440" }]
       ],
       [{ firstLetter: "V" }, 
        [{ accountName: "Visix", accountCode: "028330" }]
       ]
       
     ]; 
     this.view.sgmAccounts.setData(data); 
   },
  searchSegments:function(txtBox){
    
  }
});