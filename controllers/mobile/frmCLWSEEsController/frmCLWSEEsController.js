define({ 
  onNavigate:function(){
    this.setSegmentData();
  },

  setSegmentData: function() {
    this.view.sgmWSEEs.widgetDataMap={ 
      "lblName": "name", 
      "lblTitle": "title",
      "lblStatus": "status",
      "lblId":"id",
      "lblStatusDate":"statusDate"
    };
    var self=this;
    var data = [
      { name: "Earl Sandoval", title: "Senior Director", status:"Primed",id:"973437957", statusDate:"01/05/2017"},
      { name: "Lura Nichols", title: "Senior product manager", status:"Future Hired",id:"218398324", statusDate:"01/05/2017"},
      { name: "Pearl Ray", title: "Associate product manager", status:"Rehired",id:"859879648", statusDate:"01/05/2017"},
      { name: "Jeff Medina", title: "President", status:"Hired",id:"923749873", statusDate:"01/05/2017"},
      { name: "Troy Roberson", title: "Sr. Director", status:"LOA",id:"8274839578", statusDate:"01/05/2017"},
      { name: "Cecilia Boyd", title: "Bussiness development manager", status:"L.O.A. - Education",id:"8274839578", statusDate:"01/05/2017"},
      { name: "Aiden Hayes", title: "Chief executive officer", status:"Terminated",id:"8738567283", statusDate:"01/05/2017"},
      { name: "Jacob Berry", title: "Senior vice president", status:"Hired",id:"347365845", statusDate:"01/05/2017"},
      { name: "Kathryn Bass", title: "Director", status:"Worker's Comp",id:"3468736493", statusDate:"01/05/2017"},
    ]; 
    this.view.sgmWSEEs.setData(data); 
  },
});