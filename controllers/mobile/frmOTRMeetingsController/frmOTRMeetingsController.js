define({ 
   onNavigate: function () { 
     this.setSegmentData();
     this.setBindings();
   }, 
   setBindings:function(){
     this.view.sgmMeetings.onRowClick=this.navigateToMeetingDetail.bind(this);
   },
  navigateToMeetingDetail:function(sgmAccounts){
    var nav = new kony.mvc.Navigation("frmOTRActivityDetail");
    nav.navigate();
  },
   setSegmentData: function() {
     this.view.sgmMeetings.widgetDataMap={ 
       "lblDateNumber": "date", 
       "lblDay": "day", 
       "lblAccountName": "accountName",
       "lblSubArea":"subarea",
       "lblTime":"time",
       "lblSection":"section"
     };
     var data = [
       [
          { 
            section: "Previous weeks",
          }, 
          [
            { 
              date: "01", 
              day: "FRI",
              accountName:"Complete Pharmacy Care - 293721",
              subarea:"New Client - EE OT",
              time:"10:00 AM - 11:00 AM",
              template:"flxMeetingOld"
            },
            { 
              date: "02", 
              day: "SAT",
              accountName:"Sonora Tech - 304850",
              subarea:"New Client - EE OT",
              time:"08:00 AM - 09:00 AM",
              template:"flxMeetingOld"
            },
          ]
       ], 
       [
          { section: "This week" }, 
          [
            { 
              date: "10", 
              day: "MON",
              accountName:"Electro Accoustics & video - 9900023",
              subarea:"New Client - EE OT",
              time:"09:00 AM - 10:00 AM",
            },
            { 
              date: "12", 
              day: "TUE",
              accountName:"Texas Healthcare Linen - 937440",
              subarea:"Takeover Verify & Submission",
              time:"12:30 AM - 01:30 PM",
              template:"flxMeetingClosable"
            },
            { 
              date: "13", 
              day: "WED",
              accountName:"Texas Healthcare Linen - 937440",
              subarea:"New Client - EE OT",
              time:"05:30 PM - 06:30 PM",
              template:"flxMeetingNew"
            },
          ]
       ],
       [
          { section: "This month" }, 
          [
            { 
              date: "19", 
              day: "TUE",
              accountName:"FNB BANK",
              subarea:"Takeover Verify & Submission",
              time:"09:00 AM - 10:00 AM",
            },
            { 
              date: "20", 
              day: "WED",
              accountName:"Sonora Tech - 304850",
              subarea:"New Client - EE OT",
              time:"05:30 PM - 06:30 PM",
              template:"flxMeetingNewClosable"
            },
            { 
              date: "21", 
              day: "THU",
              accountName:"Complete Pharmacy Care",
              subarea:"Takeover Verify & Submission",
              time:"02:30 PM - 03:30 PM",
            },
          ]
       ]
     ]; 
     this.view.sgmMeetings.setData(data); 
//      var lastIndexPreviousWeek=(data[0][1].length-1);
     this.view.sgmMeetings.selectedRowIndex = [1,-1];
     
    
   },
});