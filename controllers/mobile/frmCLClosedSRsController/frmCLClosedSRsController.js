define({ 
  onNavigate:function(){
    this.setSegmentData();
  	this.setBindings();
  },
  setBindings:function(){
    this.view.sgmSRs.onRowClick=this.navigateToSRDetail.bind(this);
  },
  navigateToSRDetail:function(){
    var nav= new kony.mvc.Navigation("frmCLSRsDetail");
    nav.navigate();
  },
  setSegmentData: function() {
    this.view.sgmSRs.widgetDataMap={ 
      "lblOwner": "owner", 
      "lblArea": "area",
      "lblSubArea": "subarea",
      "lblDateTag":"dateTag",
      "lblDate":"opened",
      "lblStatus":"status",
      
    };
    var self=this;
    var data = [
      { 
       owner: "Andre Morales", 
       area: "COBRA", 
       subarea:"Takeover Verify & Submission",
       dateTag:"Opened",
       opened:"09-03-2017", 
       status:{text:"Closed",skin:"sknGreyStatus"},
      },
      { 
       owner: "Lilly Perez", 
       area: "COBRA", 
       subarea:"Takeover Verify & Submission",
       dateTag:"Opened",
       opened:"28-12-2017", 
       status:{text:"Canceled",skin:"sknGreyStatus"},
      },
      { 
       owner: "Katie Atkins", 
       area: "Client management", 
       subarea:"Quality Proactive Touch Point",
       dateTag:"Opened",
       opened:"18-02-2017", 
       status:{text:"Closed",skin:"sknGreyStatus"},
      }
    ]; 
    this.view.sgmSRs.setData(data); 
  },
});