define({ 
	_prevScreen:"",
	onNavigate:function(prevScreen){
      this._prevScreen=prevScreen;
      this.setBindings();
      this.view.preShow=this.onPreShow.bind(this);
      this.view.postShow=this.onPostShow.bind(this);
      this.setInitialConfig();
    },
  	onPreShow:function(){
      if(this._prevScreen=="activities"){
        this.selectActivity();
      }else{
        this.selectService();
      }
      this.view.dropDownAccount.closeDropdown();
      this.view.dropDownArea.closeDropdown();
      this.view.dropDownDetail.closeDropdown();
      this.view.dropDownGroup.closeDropdown();
      this.view.dropDownSubArea.closeDropdown();
    },
    onPostShow:function(){
		
    },
  	setInitialConfig:function(){
      this.view.dropDownAccount.setData(0,ACCOUNT_LIST);
      this.view.dropDownGroup.setData(0,SR_GROUP_LIST);
      this.view.dropDownDetail.setData(0,SR_DETAIL_LIST);
    },
  	setBindings:function(){
      var self=this;
      this.view.chkBoxActivity.onChange(self.selectActivity.bind(self));
      this.view.chkBoxService.onChange(self.selectService.bind(self));
      this.view.btnSubmit.onClick=this.navigateToPrev.bind(this);
	},
  	selectActivity:function(){
      this.view.dropDownArea.setData(0,ACTIVITIES_AREA_LIST);
      this.view.dropDownSubArea.setData(0,ACTIVITIES_SUBAREA_LIST);
      this.view.chkBoxActivity.setChecked();
      this.view.chkBoxService.setUnchecked();
      this.view.flxActivity.top = "150dp";
      this.view.flxServiceRequest.setVisibility(false);
    },
  	selectService:function(){
      this.view.dropDownArea.setData(0,SR_AREA_LIST);
      this.view.dropDownSubArea.setData(0,SR_SUBAREA_LIST);
      this.view.chkBoxService.setChecked();
      this.view.chkBoxActivity.setUnchecked();
      this.view.flxServiceRequest.setVisibility(true);
      this.view.flxActivity.top = "250dp";
    },
  	navigateToPrev:function(){
      var prevForm=kony.application.getPreviousForm().id;
      var nav= new kony.mvc.Navigation(prevForm);
      nav.navigate();
    }
	
 });