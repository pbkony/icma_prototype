define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnPhone **/
    AS_Button_d1cd08807c164d47a40129fec18138da: function AS_Button_d1cd08807c164d47a40129fec18138da(eventobject, context) {
        var self = this;
        return self.onPhoneClick.call(this, eventobject);
    }
});