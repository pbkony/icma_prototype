define({ 
  onNavigate:function(){
    this.setSegmentData();
    this.setBindings();
    
  },
  setBindings:function(){
    this.view.flxAdd.onClick=this.navigateToQuickAdd.bind(this);
    this.view.sgmActivities.onRowClick=this.navigateToActivityDetail.bind(this);
  },
  navigateToActivityDetail:function(){
    var nav= new kony.mvc.Navigation("frmCLActivityDetail");
    nav.navigate();
  },
  navigateToQuickAdd:function(){
    var nav = new kony.mvc.Navigation("frmQuickAdd");
    nav.navigate("activities");
  },
  setSegmentData: function() {
    this.view.sgmActivities.widgetDataMap={ 
      "lblOwner": "owner", 
      "lblArea": "area",
      "lblSubArea": "subarea",
      "lblDate":"created",
      "lblStatus":"status",
    };
    var self=this;
    var data = [
      { 
       owner: "Leila Swanson", 
       area: "Client Management", 
       subarea:"Quality Proactive Touch Point",
       created:"09-10-2017", 
       status:"Unscheduled",
       template:"flxActivitiesOwn"
      },
      { 
        owner: "Marguerite Salazar",
        area: "Client Management", 
        subarea:"Quality Proactive Touch Point",
        created:"09-10-2017", 
        status:"Assigned"
      }     
    ]; 
    this.view.sgmActivities.setData(data); 
  },
});