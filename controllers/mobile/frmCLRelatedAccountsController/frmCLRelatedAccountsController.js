define({ 

   onNavigate:function(){
     this.setSegmentData();
   },
  setSegmentData:function(){
    this.view.sgmRelatedAccounts.widgetDataMap={ 
       "lblSection": "group", 
       "lblAccountName": "accountName", 
       "lblAccountCode": "accountCode",
       "imgPrincipal": "imgPrincipal",
       "flxDivision":"division"};
       
     var highlightedTemplate=this.view.flxHighlitedAccount;
     var data = [
       [{ group: "Common Ownership Group 1",division:{skin:"flxWhite"} }, 
        [{ accountName: "FNB Bank", accountCode: "2001290" }, 
         { accountName: {text:"Sonora Tech",skin:"sknLblRowHighlighted"},
           accountCode: {text:"304850",skin:"sknLblRowHighlighted"},
           imgPrincipal:{isVisible:true,src:"principal_icon.png"}
         },
         { accountName: "ABC Widgets", accountCode: "3001200" },
         { accountName: "Nortech Systems", accountCode: "900029" }]
       ], 
       [{ group: "Common Ownership Group 2" }, 
        [{ accountName: "Visix", accountCode: "028330" },
        { accountName: {text:"Stratitia",skin:"sknLblRowHighlighted"},
           accountCode: {text:"100283",skin:"sknLblRowHighlighted"},
           imgPrincipal:{isVisible:true,src:"principal_icon.png"}
         },
        { accountName: "Access Speciality Animal Hospital", accountCode: "200384" }]
       ]
     ]; 
     this.view.sgmRelatedAccounts.setData(data); 
  }

 });