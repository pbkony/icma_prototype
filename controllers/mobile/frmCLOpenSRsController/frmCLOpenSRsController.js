define({ 
  onNavigate:function(){
    this.setSegmentData();
    this.setBindings();
  },
  setBindings:function(){
    this.view.flxAdd.onClick=this.navigateToQuickAdd.bind(this);
    this.view.sgmSRs.onRowClick=this.navigateToSRDetail.bind(this);
  },
  navigateToSRDetail:function(){
    var nav= new kony.mvc.Navigation("frmCLSRsDetail");
    nav.navigate();
  },
  navigateToQuickAdd:function(){
    var nav = new kony.mvc.Navigation("frmQuickAdd");
    nav.navigate("srs");
  },
  setSegmentData: function() {
    this.view.sgmSRs.widgetDataMap={ 
      "lblOwner": "owner", 
      "lblArea": "area",
      "lblSubArea": "subarea",
      "lblDate":"opened",
      "lblStatus":"status",
      
    };
    var self=this;
    var data = [
      { 
       owner: "Franklin Douglas", 
       area: "COBRA", 
       subarea:"Takeover Verify & Submission",
       opened:"09-03-2017", 
       status:"Unscheduled",
      },
      { 
       owner: "Elnora Marsh", 
       area: "COBRA", 
       subarea:"Takeover Verify & Submission",
       opened:"28-12-2017", 
       status:"Unscheduled",
       template:"flxSRsOwn"
      },
      { 
        owner: "Theodore Rogers",
        area: "Client management", 
        subarea:"Quality Proactive Touch Point",
        opened:"09-02-2017", 
        status:{text:"Pending",skin:"sknRedStatus"}
      }     
    ]; 
    this.view.sgmSRs.setData(data); 
  },
});