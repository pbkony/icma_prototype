define({ 
  onNavigate:function(){
    this.setSegmentData();
	this.setBindings(); 
  },
  setBindings:function(){
    this.view.sgmActivities.onRowClick=this.navigateToActivityDetail.bind(this);
  },
  navigateToActivityDetail:function(){
    var nav= new kony.mvc.Navigation("frmCLActivityDetail");
    nav.navigate();
  },
  setSegmentData: function() {
    this.view.sgmActivities.widgetDataMap={ 
      "lblOwner": "owner", 
      "lblArea": "area",
      "lblSubArea": "subarea",
      "lblDate":"opened",
      "lblStatus":"status",
      
    };
    var self=this;
    var data = [
      { 
       owner: "Franklin Douglas", 
       area: "Client Managment", 
       subarea:"Quality Proactive Touch Point",
       opened:"09-10-2017", 
       status:{text:"Done",skin:"sknGreyStatus"},
      },
      { 
       owner: "Mildred Morales", 
       area: "Client Management", 
       subarea:"Quality Proactive Touch Point",
       opened:"09-10-2017", 
       status:{text:"Cancelled",skin:"sknGreyStatus"},
      }
    ]; 
    this.view.sgmActivities.setData(data); 
  },
});