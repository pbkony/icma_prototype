define({ 
  onViewCreated:function(){
    this.view.btnQuickUpdate.onClick=this.showQuickUpdate.bind(this);
  },
  showQuickUpdate:function(eventObj){
    var QUWindow=new Common.QuickUpdateSR({
      "clipBounds": true,
      "isMaster": true,
      "height": "100%",
      "id": "QuickUpdate",
      "isVisible": true,
      "layoutType": kony.flex.FREE_FORM,
      "left": "0dp",
      "skin": "slFbox",
      "top": "0dp",
      "width": "100%",
      "zIndex": 10
    }, {}, {});
    var currentForm=kony.application.getCurrentForm();
	QUWindow.setCustomCloseCallback(()=>{
      currentForm.remove(QUWindow);
    });
    currentForm.add(QUWindow);
    
  }
 });